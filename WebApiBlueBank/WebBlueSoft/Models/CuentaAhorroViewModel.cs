﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebBlueSoft.Models
{
    public class CuentaAhorroViewModel
    {
        [Display(Name = "Numero De Cuenta de Ahorro")]
        public int NumeroCuenta { get; set; }

        [Required(ErrorMessage = "El campo es Obligatorio")]
        [Display(Name = "Nombre Completo del titular")]
        public string NombreTitular { get; set; }

        [Display(Name = "Valor Inicial")]
        public decimal ValorInicial { get; set; }

        [Display(Name = "Valor de consignación")]
        public decimal? ValorConsignacion { get; set; }

        [Display(Name = "Valor de Retiro")]
        public decimal? ValorRetiro { get; set; }

        public ICollection<CuentaAhorroViewModel> Data { get; set; }
    }
}
