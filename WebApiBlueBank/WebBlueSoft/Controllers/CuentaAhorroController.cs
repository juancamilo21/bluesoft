﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using WebBlueSoft.Helper;
using WebBlueSoft.Models;

namespace WebBlueSoft.Controllers
{
    public class CuentaAhorroController : Controller
    {
        WebApi _api = new WebApi();


        public async Task<IActionResult> Index()
        {
            IEnumerable<CuentaAhorroViewModel> CuentaAhorro = null;
            HttpClient client = _api.Inicial();

            HttpResponseMessage res = await client.GetAsync("api/CuentaAhorro");
            if (res.IsSuccessStatusCode)
            {
                var results = res.Content.ReadAsStringAsync().Result;
                CuentaAhorro = JsonConvert.DeserializeAnonymousType (results, new { data = default(List<CuentaAhorroViewModel>) }).data;
            }
            return View(CuentaAhorro);
        }

        public async Task<IActionResult> Details(int Id)
        {

            var CuentaAhorro = new CuentaAhorroViewModel();
            HttpClient client = _api.Inicial();
            HttpResponseMessage res = await client.GetAsync($"api/CuentaAhorro/{Id}");
            if (res.IsSuccessStatusCode)
            {
                var results = res.Content.ReadAsStringAsync().Result;
                CuentaAhorro = JsonConvert.DeserializeAnonymousType(results, new { data = default(CuentaAhorroViewModel) }).data;
            }
            return View(CuentaAhorro);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(CuentaAhorroViewModel CuentaAhorro)
        {
            HttpClient client = _api.Inicial();

            var postTask = client.PostAsJsonAsync<CuentaAhorroViewModel>("api/CuentaAhorro", CuentaAhorro);
            postTask.Wait();

            var result = postTask.Result;
            if (result.IsSuccessStatusCode)
            {
                return RedirectToAction("Index");
            }

            return View();
        }

        public async Task<IActionResult> Delete(int Id)
        {
            var Code = new CuentaAhorroViewModel();
            HttpClient client = _api.Inicial();
            HttpResponseMessage res = await client.DeleteAsync($"api/CuentaAhorro/{Id}");

            return RedirectToAction("Index");
        }

        public async Task<ActionResult> Edit(int Id)
        {
            var CuentaAhorro = new CuentaAhorroViewModel();
            HttpClient client = _api.Inicial();
            HttpResponseMessage res = await client.GetAsync($"api/CuentaAhorro/{Id}");
            if (res.IsSuccessStatusCode)
            {
                var results = res.Content.ReadAsStringAsync().Result;
                CuentaAhorro = JsonConvert.DeserializeAnonymousType(results, new { data = default(CuentaAhorroViewModel)}).data;
                
            }
            return View(CuentaAhorro);

        }

        [HttpPost]
        public ActionResult Edit(CuentaAhorroViewModel CuentaAhorro)
        {
            HttpClient client = _api.Inicial();
            var putTask = client.PutAsJsonAsync<CuentaAhorroViewModel>($"api/CuentaAhorro", CuentaAhorro);
            putTask.Wait();

            var result = putTask.Result;
            if (result.IsSuccessStatusCode)
                return RedirectToAction("Index");
            return View(CuentaAhorro);


        }
        public async Task<ActionResult> Consignacion(int Id)
        {
            var CuentaAhorro = new CuentaAhorroViewModel();
            HttpClient client = _api.Inicial();
            HttpResponseMessage res = await client.GetAsync($"api/CuentaAhorro/{Id}");
            if (res.IsSuccessStatusCode)
            {
                var results = res.Content.ReadAsStringAsync().Result;
                CuentaAhorro = JsonConvert.DeserializeAnonymousType(results, new { data = default(CuentaAhorroViewModel) }).data;

            }
            return View(CuentaAhorro);

        }

        [HttpPost]
        public ActionResult Consignacion(CuentaAhorroViewModel CuentaAhorro)
        {
            HttpClient client = _api.Inicial();
            var putTask = client.PutAsJsonAsync<CuentaAhorroViewModel>($"api/CuentaAhorro", CuentaAhorro);
            putTask.Wait();

            var result = putTask.Result;
            if (result.IsSuccessStatusCode)
                return RedirectToAction("Index");
            return View(CuentaAhorro);

        }
        }
    }