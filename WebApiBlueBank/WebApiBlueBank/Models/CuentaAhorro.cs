﻿using System;
using System.Collections.Generic;

namespace WebApiBlueBank.Models
{
    public partial class CuentaAhorro
    {
        public int NumeroCuenta { get; set; }
        public string NombreTitular { get; set; }
        public decimal ValorInicial { get; set; }
        public decimal? ValorConsignacion { get; set; }
        public decimal? ValorRetiro { get; set; }
    }
}
