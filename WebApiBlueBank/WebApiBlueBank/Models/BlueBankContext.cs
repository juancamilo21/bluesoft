﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace WebApiBlueBank.Models
{
    public partial class BlueBankContext : DbContext
    {
        public BlueBankContext()
        {
        }

        public BlueBankContext(DbContextOptions<BlueBankContext> options)
            : base(options)
        {
        }

        public virtual DbSet<CuentaAhorro> CuentaAhorro { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {

                optionsBuilder.UseSqlServer("Server=localhost;Database=BlueBank;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.6-servicing-10079");

            modelBuilder.Entity<CuentaAhorro>(entity =>
            {
                entity.HasKey(e => e.NumeroCuenta);

                entity.Property(e => e.NumeroCuenta).ValueGeneratedNever();

                entity.Property(e => e.NombreTitular)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ValorConsignacion).HasColumnType("money");

                entity.Property(e => e.ValorInicial).HasColumnType("money");

                entity.Property(e => e.ValorRetiro).HasColumnType("money");
            });
        }
    }
}
