﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApiBlueBank.DTO;

namespace WebApiBlueBank.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("permitir")]
    public class CuentaAhorroController : ControllerBase
    {
        private Models.BlueBankContext _db;
        private static readonly Random Rng = new Random();

        public CuentaAhorroController(Models.BlueBankContext db)
        {
            _db = db;
        }


        [HttpGet]
        public ActionResult<IEnumerable<string>> GetAll()
        {
            Response oRespuesta = new Response();
            try
            {
                var lst = from d in _db.CuentaAhorro.OrderByDescending(d=>d.NumeroCuenta)
                          select d;
                oRespuesta.Exito = 1;
                oRespuesta.Data = lst;

            }
            catch (Exception ex)
            {

                oRespuesta.Mensaje = ex.Message;
            }
            

            return Ok(oRespuesta);
        }

        [HttpGet("{id}")]
        public ActionResult<IEnumerable<string>> Get(int id)
        {
            Response oRespuesta = new Response();
            try
            {
                var lst = (from d in _db.CuentaAhorro
                           where id == d.NumeroCuenta
                           select d).SingleOrDefault();
                oRespuesta.Exito = 1;
                oRespuesta.Data = lst;
            }
            catch (Exception ex)
            {

                oRespuesta.Mensaje = ex.Message;
            }


            return Ok(oRespuesta);


        }

        [HttpPost]
        public ActionResult Post([FromBody] CuentaAhorroDTO model)
        {
            Response oRespuesta = new Response();

            try
            {
                Models.CuentaAhorro oCuenta = new Models.CuentaAhorro();
                oCuenta.NumeroCuenta = Rng.Next(100000000,999999999);
                oCuenta.NombreTitular = model.NombreTitular;
                oCuenta.ValorInicial = model.ValorInicial;
                oCuenta.ValorConsignacion = model.ValorConsignacion;
                oCuenta.ValorRetiro = model.ValorRetiro;

                _db.CuentaAhorro.Add(oCuenta);
                _db.SaveChanges();
                oRespuesta.Exito = 1;
            }
            catch (Exception ex)
            {

                oRespuesta.Mensaje = ex.Message;
                
            }
            return Ok(oRespuesta);
        }

        [HttpPut]
        public ActionResult Put([FromBody]  CuentaAhorroDTO model)
        {
            Response oRespuesta = new Response();
            try
            {

                Models.CuentaAhorro oCuenta = _db.CuentaAhorro.Find(model.NumeroCuenta);
                oCuenta.NombreTitular = model.NombreTitular;
                oCuenta.ValorInicial = model.ValorInicial;
                oCuenta.ValorConsignacion = model.ValorConsignacion;
                oCuenta.ValorRetiro = model.ValorRetiro;
                _db.Entry(oCuenta).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                _db.SaveChanges();
                oRespuesta.Exito = 1;
            }
            catch (Exception ex)
            {

                oRespuesta.Mensaje = ex.Message;

            }
            return Ok(oRespuesta);
        }
        

        [HttpDelete("{id}")]
        public ActionResult Delete(int Id)
        {
            Response oRespuesta = new Response();
            try
            {

                Models.CuentaAhorro oCuenta = _db.CuentaAhorro.Find(Id);
                _db.CuentaAhorro.Remove(oCuenta);
                _db.SaveChanges();

                oRespuesta.Exito = 1;
            }
            catch (Exception ex)
            {

                oRespuesta.Mensaje = ex.Message;

            }
            return Ok(oRespuesta);
        }
    }
}