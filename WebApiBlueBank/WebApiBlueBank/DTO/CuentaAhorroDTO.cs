﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApiBlueBank.DTO
{
    public class CuentaAhorroDTO
    {
        public int NumeroCuenta { get; set; }
        public string NombreTitular { get; set; }
        public decimal ValorInicial { get; set; }
        public decimal? ValorConsignacion { get; set; }
        public decimal? ValorRetiro { get; set; }
    }
}
